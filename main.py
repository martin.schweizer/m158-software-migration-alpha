# -------------------------------------------------------------------------------
# Project:     ALPHA
# Name:        main.py
# Purpose:     Generiert die Ausgangsdatenbank
# Description: Generate new SQLite Database (Old solution)
#
#
# Author:      swisi
#
# Created:     07.06.2022
# Copyright:   (c) swisi 2022
# Licence:     MIT | https://mit-license.org/
# -------------------------------------------------------------------------------



from curses.ascii import isdigit
import sqlite3
from sqlite3 import Error

import sys
import logging
import requests
import random
import uuid

from faker import Faker

# Datenbanklasse
class dbInit:
    def __init__(self):
        pass

    def testdb():   
        
        antwort=False        

        conn = dbInit.create_connection()
        cursor = conn.cursor()

        stmtu = ("SELECT EXISTS(SELECT 1 FROM users)")
        stmtr = ("SELECT EXISTS(SELECT 1 FROM results)")
        
        try:
            cursor.execute(stmtu)
            if cursor.fetchone():
                Eventlog.event(10,"User-Datensatz gefunden!")
                antwort = True
            else:
                Eventlog.event(10,"Keine User-Daten vorhanden")
                antwort = False
        except Error as e:
            Eventlog.event(10,e)
        
        try:
            cursor.execute(stmtr)
            if cursor.fetchone():
                Eventlog.event(10,"Result-Datensatz gefunden!")
                antwort = True
            else:
                Eventlog.event(10,"Keine Result-Daten vorhanden")
                antwort = False
        except Error as e:
            Eventlog.event(10,e)
            
        Eventlog.event(10,"Antwort ist " + str(antwort))
        return antwort

    def create_connection():

        conn = None

        try:
            conn = sqlite3.connect(r"sqlite.db")
            return conn
        except Error as e:
            print(e)

        return conn

    def create_dbtable(sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """

        sql_create_table = sql
        conn = dbInit.create_connection()

        # create tables
        if conn is not None:
            # create projects table
            conn.execute(sql_create_table)

        else:
            print("Error! cannot create the database connection.")

        conn.close()

    def drop_dbtables(sql):

        conn = dbInit.create_connection()
        cursor = conn.cursor()

        # drop table from sql
        if conn is not None:

            cursor.execute(sql)
            conn.commit()
        else:
            print("Error! cannot create the database connection.")

        conn.close()

    def create_table():

        sql = "DROP TABLE IF EXISTS users"
        dbInit.drop_dbtables(sql)

        sql = """ CREATE TABLE IF NOT EXISTS users (
                    Uid integer PRIMARY KEY,
                    User text NOT NULL,
                    Vorname text NOT NULL,
                    Name text NOT NULL,
                    Adresse text NOT NULL,
                    PLZ text NOT NULL,
                    Ort text NOT NULL,
                    Land text NOT NULL,
                    Mail text NOT NULL
              ); """

        dbInit.create_dbtable(sql)

        sql = "DROP TABLE IF EXISTS results"
        dbInit.drop_dbtables(sql)

        sql = """CREATE TABLE IF NOT EXISTS results (
                    id integer PRIMARY KEY,
                    user_id integer NOT NULL,
                    game text NOT NULL,
                    level integer,
                    score integer NOT NULL,
                    ranking integer NOT NULL,
                    FOREIGN KEY (user_id) REFERENCES users (uid)
                );"""

        dbInit.create_dbtable(sql)

    def drop_table():

        sql = "DROP TABLE IF EXISTS users"
        dbInit.drop_dbtables(sql)

        sql = "DROP TABLE IF EXISTS results"
        dbInit.drop_dbtables(sql)

    def generate_users():

        fake = Faker()

        vorname     = UserGenerate.get_first()
        name        = UserGenerate.get_last()
        strasse     = UserGenerate.get_street()
        plz, city   = UserGenerate.get_city()

        user = []

        conn = dbInit.create_connection()

        for i in range(189):

            t = False
            uid = str(uuid.uuid4())[:8]
            while t == False:
                if uid.isdigit():
                    t = False
                    uid = str(uuid.uuid4())[:8]
                    Eventlog.event(10, "Generierte UserID war numerisch!")
                else:
                    t = True
                
            user.append(i)
            user.append(uid)
            user.append((random.choice(vorname)))
            user.append((random.choice(name)))
            user.append((random.choice(strasse)))
            user.append((random.choice(plz)))
            user.append((random.choice(city)))
            user.append("Switzerland")
            user.append(fake.email())

            params = ['?' for item in user]
            sql    = 'INSERT INTO users (UID,User,Vorname,Name,Adresse,PLZ,Ort,Land,Mail) VALUES (%s);' % ','.join(params)
            conn.execute(sql, user)

            user = []


        conn.commit()
        conn.close()

    def generate_result():


        conn = dbInit.create_connection()
        cursor = conn.cursor()

        list_of_user = []
        list_of_game = ["Minecraft","GTA V","Tetris","Overwatch","Red Dead 2", "Call of Duty", "Wolfenstein"]
        list_of_result = []

        abfrage = cursor.execute("Select User from users")
        spieler = abfrage.fetchall()

        for row in spieler:
            for field in row:
                list_of_user.append(field)
                
        #print(list_of_user)

        anz_user = len(list_of_user)

        for i in range(0,2761493):
            list_of_result.append(i)                                            #id
            list_of_result.append(str((random.choice(list_of_user))))           #user_id
            list_of_result.append(str((random.choice(list_of_game))))                #game
            list_of_result.append(random.randint(1, 100))                       #level
            list_of_result.append(random.randint(5000, 50000))                  #score
            list_of_result.append(random.randint(1,anz_user))                   #ranking

            sql = 'INSERT INTO results(id,user_id,game,level,score,ranking) VALUES(?,?,?,?,?,?)'

        #    print(list_of_result )
            cursor.execute(sql, list_of_result)

            list_of_result=[]

        conn.commit()
        conn.close()

    def startInit():

        Eventlog.event(20,"Initialisierung gestartet!")

        if dbInit.testdb() != True:
            Eventlog.event(10, "Datenbank muss initialisiert werden")
            dbInit.create_table()
            dbInit.generate_users()
            dbInit.generate_result()
        else:
            Eventlog.event(10, "Datenbank schon vorhanden")

        Eventlog.event(20, "Initialisierung abgeschlossen")

# UserGenerate
class UserGenerate:
    def __init__(self):
        pass

    def get_first():

        r = requests.get(url="https://swisspost.opendatasoft.com/api/records/1.0/search/?dataset=vornamen_proplz&q=&facet=vorname&rows=10000")

        Eventlog.event(20, "Status: " + str(r.status_code) + " Vorname OK")

        data = r.json()

        vorname = []

        # extracting latitude, longitude and formatted address of the first matching location
        for i in range(len(data['records'])):
            first = data['records'][i]['fields']['vorname']
            if first == 'n/a':
                continue
            elif first in vorname:
                continue
            else:
                vorname.append(first)

            if len(vorname) >= 50:
                break


        return vorname

    def get_last():

        r = requests.get(url="https://swisspost.opendatasoft.com/api/records/1.0/search/?dataset=nachnamen_proplz&q=&facet=nachname&rows=10000")

        Eventlog.event(20, "Status: " + str(r.status_code) + " Nachname OK")
        data = r.json()

        nachname = []

        # extracting latitude, longitude and formatted address of the first matching location
        for i in range(len(data['records'])):
            last = data['records'][i]['fields']['nachname']
            if last == 'n/a':
                continue
            elif last in nachname:
                continue
            else:
                nachname.append(last)

            if len(nachname) >= 50:
                break


        return nachname

    def get_street():

        r = requests.get(url="https://swisspost.opendatasoft.com/api/records/1.0/search/?dataset=strassenbezeichnungen_v2&q=&facet=strbezl&rows=10000")

        Eventlog.event(20, "Status: " + str(r.status_code) + " Strasse OK")

        data = r.json()

        strasse = []

        # extracting latitude, longitude and formatted address of the first matching location
        for i in range(len(data['records'])):
            street = data['records'][i]['fields']['strbezl']
            if street == 'n/a':
                continue
            elif street in strasse:
                continue
            else:
                strasse.append(street)

            if len(strasse) >= 50:
                break


        return strasse

    def get_city():

        r = requests.get(url="https://swisspost.opendatasoft.com/api/records/1.0/search/?dataset=plz_verzeichnis_v2&q=&facet=postleitzahl&facet=ortbez18&rows=10000")

        Eventlog.event(20, "Status: " + str(r.status_code) + " Postleitzahl & Ort OK")

        data = r.json()

        plz = []
        city = []

        # extracting Postleitzahl
        for i in range(len(data['records'])):
            postleitzahl = data['records'][i]['fields']['postleitzahl']
            if plz == 'n/a':
                continue
            elif postleitzahl in plz:
                continue
            else:
                plz.append(postleitzahl)

            if len(plz) >= 50:
                break

        # extracting Postleitzahl
        for i in range(len(data['records'])):
            ort = data['records'][i]['fields']['ortbez27']
            if city == 'n/a':
                continue
            elif ort in city:
                continue
            else:
                city.append(ort)

            if len(city) >= 50:
                break


        return plz, city

# Helper Klasse
class Eventlog:
    def __init__(self):
        pass

    def eventLogInit():
        logging.basicConfig(
            #level=logging.DEBUG,
            level=logging.INFO,
            format="%(asctime)s [%(levelname)s] %(message)s",
            handlers=[
                logging.FileHandler("alpha.log",mode='w'),
                logging.StreamHandler(sys.stdout)
            ]
        )

    def event( lvl, text):
        """"
        Level 0     = NOTSET
        Level 10    = DEBUG
        Level 20    = INFO
        Level 30    = WARNING
        Level 40    = ERROR
        Level 50    = CRITICAL
        """
        logging.log(lvl, text)


# Initialisiert das Logging
Eventlog.eventLogInit()

# Loggt den Anwendungsstart
Eventlog.event(20, "Anwendung gestartet")
Eventlog.event(20, "Please be patient!")

# Prüft ob eine Initialisierung erfoderlich resp. erstellt die DB
dbInit.startInit()

# Loggt den Anwendungsschluss
Eventlog.event(20, "Anwendung beendet")



