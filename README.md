# M158 Software-Migration | ALPHA

## Projektarbeit:

### Ausgangslage:

Die Firma EduGame AG verfügt über eine Vielzahl von verteilten Systemen. 

Eine der ersten Systeme ist eine Spielerverwaltung. Diese Anwendung wurde unter Python erstellt
und diente bis heute lediglich zum Sammeln und Speichern von Spieler und Spielständen.

Leider ist diese Lösung aus verschiedensten Gründen nicht länger tragbar. Die Geschäftsleitung hat daher entschieden, dass die Anwendung von Grund auf erneuert und durch eine ansprechende Weblösung ersetzt werden soll.

Der Entwickler, der mit der Erneuerung betraut ist hat bereits begonnen ein Grundgerüst zu erstellen.
Damit die Entwicklung weitergeführt werden kann, bittet er Sie als Projektleiter um Hilfe. Er ist darauf angewiesen, dass er möglichst rasch mit fiktiven Testdaten arbeiten kann.

Sein Anliegen stösst bei Ihnen auf offene Ohren, denn im Zuge der Erneuerung hat man Sie mit der Migration der bestehenden Daten betraut. Somit können Sie im Rahmen der Anfrage gleich die Migration der Datenbank konzipieren, das Vorgehen der Migration planen, erforderliche Aufgaben realisieren und ihre Umsetzung  testen.

## ✨ How to use it

> **Step 1** - Installiere `Python3`

- Installiere die aktuelle Version von Python3 z.B. in einer Windows oder Ubuntu VM 
(Quelle: https://www.python.org/) 

<br />

> **Step 2** - Clone this Repo

- Klone das GIT-Repository auf diesen virtuellen Rechner <br>
`git clone git@gitlab.com:martin.schweizer/m158-software-migration-alpha.git`
- Wechsle in das neue Unterverzeichnis und Installiere die erforderliche Abhängigkeiten <br>
`pip install -r requirements.txt`

<br />

> **Step 3** - Run Application

- Führe anschliessend das Programm main.py aus <br>
`python -m main.py`
